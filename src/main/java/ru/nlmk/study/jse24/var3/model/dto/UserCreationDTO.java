package ru.nlmk.study.jse24.var3.model.dto;

public class UserCreationDTO {
    private String FirstName;
    private Integer age;
    private String password;

    public UserCreationDTO(String firstName, Integer age, String password) {
        FirstName = firstName;
        this.age = age;
        this.password = password;
    }

    public String getFirstName() {
        return FirstName;
    }

    public Integer getAge() {
        return age;
    }

    public String getPassword() {
        return password;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
