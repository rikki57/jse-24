package ru.nlmk.study.jse24.var3;

import org.mapstruct.factory.Mappers;
import ru.nlmk.study.jse24.var3.model.dto.UserCreationDTO;
import ru.nlmk.study.jse24.var3.model.entity.User;
import ru.nlmk.study.jse24.var3.model.mapper.UserMapper;

public class Main {
    public static void main(String[] args) {
        UserCreationDTO userCreationDTO = new UserCreationDTO("Mike", 22, "sfsfwqdfsdf8s7f87s6df");
        User user = Mappers.getMapper(UserMapper.class).userCreationDTOToUser(userCreationDTO);
        System.out.println(user);
    }
}
