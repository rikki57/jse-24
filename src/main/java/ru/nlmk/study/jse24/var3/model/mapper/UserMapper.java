package ru.nlmk.study.jse24.var3.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.nlmk.study.jse24.var3.model.dto.UserCreationDTO;
import ru.nlmk.study.jse24.var3.model.entity.User;

@Mapper
public interface UserMapper {
    @Mapping(source = "firstName", target = "name")
    User userCreationDTOToUser(UserCreationDTO userCreationDTO);
}
