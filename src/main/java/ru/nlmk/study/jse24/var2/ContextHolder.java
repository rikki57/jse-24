package ru.nlmk.study.jse24.var2;

import java.util.HashMap;
import java.util.Map;

public class ContextHolder {
    private Map<String, ContextItem> context;

    public ContextHolder() {
        context = new HashMap<>();
    }


    public Object initObject(String name, Object instance){
        try {
            DependencyInjector.inject(context, instance);
            context.put(name, new ContextItem(name, instance.getClass(), instance));
        } catch (UnableToInitException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return instance;
    }

    public Map<String, ContextItem> getContext() {
        return context;
    }
}
