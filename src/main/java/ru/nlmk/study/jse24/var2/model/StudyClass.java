package ru.nlmk.study.jse24.var2.model;

import ru.nlmk.study.jse24.var1.Collect;
import ru.nlmk.study.jse24.var1.CollectType;

public class StudyClass {
    private String name;
    private Integer students;

    public StudyClass(String name, Integer students) {
        this.name = name;
        this.students = students;
    }

    @Override
    public String toString() {
        return "StudyClass{" +
                "name='" + name + '\'' +
                ", students=" + students +
                '}';
    }
}
