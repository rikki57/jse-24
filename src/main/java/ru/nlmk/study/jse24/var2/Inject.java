package ru.nlmk.study.jse24.var2;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
    InjectionMethod value() default InjectionMethod.BY_TYPE;
    String name() default "";
}
