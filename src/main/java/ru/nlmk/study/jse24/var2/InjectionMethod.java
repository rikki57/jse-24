package ru.nlmk.study.jse24.var2;

public enum InjectionMethod {
    BY_TYPE,
    BY_NAME
}
