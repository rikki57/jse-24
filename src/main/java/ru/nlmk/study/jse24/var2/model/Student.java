package ru.nlmk.study.jse24.var2.model;

import ru.nlmk.study.jse24.var1.Collect;
import ru.nlmk.study.jse24.var1.CollectType;
import ru.nlmk.study.jse24.var2.Inject;
import ru.nlmk.study.jse24.var2.InjectionMethod;

public class Student {
    private String name;
    private String lastName;
    private Integer age;
    private String city;
    @Inject(value = InjectionMethod.BY_NAME, name = "8b")
    private StudyClass studyClass;

    public Student(String name, String lastName, Integer age, String city) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                ", studyClass=" + studyClass +
                '}';
    }
}
