package ru.nlmk.study.jse24.var2;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DependencyInjector {
    public static void inject(Map<String, ContextItem> context, Object instance) throws UnableToInitException, IllegalAccessException {
        for (Field field : instance.getClass().getDeclaredFields()){
            if (field.isAnnotationPresent(Inject.class)){
                Inject inject = field.getAnnotation(Inject.class);
                Object instanceToInject = null;
                if (inject.value().equals(InjectionMethod.BY_NAME)){
                    String name = inject.name();
                    ContextItem contextItem = context.get(name);
                    if (contextItem != null){
                        instanceToInject = contextItem.getInstance();
                    } else {
                        throw new UnableToInitException("No injection candidate");
                    }
                } else {
                    List<ContextItem> injectionCandidates = getInjectionCandidatesByType(field.getType(), context);
                    if (injectionCandidates.size() != 1){
                        throw new UnableToInitException("No or too many injection candidates");
                    }
                    instanceToInject = injectionCandidates.get(0).getInstance();
                }
                if (instanceToInject == null){
                    throw new UnableToInitException("Injection error");
                }
                field.setAccessible(true);
                field.set(instance, instanceToInject);
            }
        }
    }

    private static List<ContextItem> getInjectionCandidatesByType(Class<?> fieldClass, Map<String, ContextItem> context) {
        List<ContextItem> result = new ArrayList<>();
        for (Map.Entry<String, ContextItem> entry : context.entrySet()){
            if (entry.getValue().getType().equals(fieldClass)){
                result.add(entry.getValue());
            }
        }
        return result;
    }
}
