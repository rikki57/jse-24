package ru.nlmk.study.jse24.var2;

public class ContextItem {
    private String name;
    private Class type;
    private Object instance;

    public ContextItem(String name, Class type, Object instance) {
        this.name = name;
        this.type = type;
        this.instance = instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public Object getInstance() {
        return instance;
    }

    public void setInstance(Object instance) {
        this.instance = instance;
    }

    @Override
    public String toString() {
        return "ContextItem{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", instance=" + instance +
                '}';
    }
}
