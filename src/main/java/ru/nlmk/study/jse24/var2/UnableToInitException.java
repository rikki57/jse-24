package ru.nlmk.study.jse24.var2;

public class UnableToInitException extends Exception {
    public UnableToInitException() {
    }

    public UnableToInitException(String message) {
        super(message);
    }
}
