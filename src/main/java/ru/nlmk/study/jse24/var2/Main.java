package ru.nlmk.study.jse24.var2;

import ru.nlmk.study.jse24.var2.model.Student;
import ru.nlmk.study.jse24.var2.model.StudyClass;

public class Main {
    public static void main(String[] args) {
        ContextHolder contextHolder = new ContextHolder();
        contextHolder.initObject("8a", new StudyClass("8a", 30));
        contextHolder.initObject("8b", new StudyClass("8b", 35));

        contextHolder.initObject("James", new Student("James", "Sullivan", 4, "New York"));
        System.out.println(contextHolder.getContext());
    }
}
