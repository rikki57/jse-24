package ru.nlmk.study.jse24.var1;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Collector {
    public List<FieldDescriptor> collectDescriptions(List<Object> objects) throws IllegalAccessException {
        Map<String, FieldDescriptor> result = new HashMap<String, FieldDescriptor>();
        for (Object object : objects){
            Class instanceClass = object.getClass();
            for (Field field : instanceClass.getDeclaredFields()){
                if (field.isAnnotationPresent(Collect.class)){
                    Collect collect = field.getAnnotation(Collect.class);
                    field.setAccessible(true);
                    addValue(result, field.get(object), instanceClass.getName() + "." + field.getName(),
                            field.getType(), collect.value());
                }
            }
        }
        return new ArrayList<FieldDescriptor>(result.values());
    }

    private void addValue(Map<String, FieldDescriptor> currentState, Object value, String fieldName, Class<?> fieldType, CollectType collectType) {
        FieldDescriptor currentFieldDescriptor = currentState.get(fieldName);
        currentState.put(fieldName, getFieldDescriptor(fieldName, fieldType, collectType, currentFieldDescriptor, value));
    }

    private FieldDescriptor getFieldDescriptor(String fieldName, Class<?> fieldType, CollectType collectType, FieldDescriptor currentFieldDescriptor, Object value) {
        switch (collectType){
            case FLAG_ONLY:
                if (currentFieldDescriptor == null || (value != null && !currentFieldDescriptor.isAtLeastOneValue())){
                    return new FieldDescriptor(fieldName, fieldType, value != null, null);
                } else {
                    return currentFieldDescriptor;
                }
            case LAST_VALUE_ONLY:
                if (value != null){
                    return new FieldDescriptor(fieldName, fieldType, false, value.toString());
                } else {
                    return currentFieldDescriptor;
                }
            case FLAG_AND_LAST_VALUE:
                if (value != null){
                    return new FieldDescriptor(fieldName, fieldType, true, value.toString());
                } else {
                    return currentFieldDescriptor;
                }
        }
        return null;
    }
}
