package ru.nlmk.study.jse24.var1;

public class FieldDescriptor {
    private String fieldName;
    private Class fieldType;
    private boolean atLeastOneValue;
    private String lastValueStringRepresentation;

    public FieldDescriptor(String fieldName, Class fieldType, boolean atLeastOneValue, String lastValueStringRepresentation) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.atLeastOneValue = atLeastOneValue;
        this.lastValueStringRepresentation = lastValueStringRepresentation;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Class getFieldType() {
        return fieldType;
    }

    public void setFieldType(Class fieldType) {
        this.fieldType = fieldType;
    }

    public boolean isAtLeastOneValue() {
        return atLeastOneValue;
    }

    public void setAtLeastOneValue(boolean atLeastOneValue) {
        this.atLeastOneValue = atLeastOneValue;
    }

    public String getLastValueStringRepresentation() {
        return lastValueStringRepresentation;
    }

    public void setLastValueStringRepresentation(String lastValueStringRepresentation) {
        this.lastValueStringRepresentation = lastValueStringRepresentation;
    }

    @Override
    public String toString() {
        return "Description{" +
                "fieldName='" + fieldName + '\'' +
                ", fieldType=" + fieldType +
                ", atLeastOneValue=" + atLeastOneValue +
                ", lastValueStringRepresentation='" + lastValueStringRepresentation + '\'' +
                '}';
    }
}
