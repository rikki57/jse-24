package ru.nlmk.study.jse24.var1;

import ru.nlmk.study.jse24.var1.model.Student;
import ru.nlmk.study.jse24.var1.model.StudyClass;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        Student student1 = new Student("Mike", "Wazovsky", 4, "New York");
        Student student2 = new Student("James", "Sullivan", 5, "New York");
        Student student3 = new Student("Randall", "Boggs", 4, "New York");
        StudyClass studyClass1 = new StudyClass("8a", 40);
        StudyClass studyClass2 = new StudyClass("8b", 44);
        List<Object> values = Arrays.asList(student1, student2, student3, studyClass2, studyClass1);
        try {
            List<FieldDescriptor> fieldDescriptors = new Collector().collectDescriptions(values);
            logger.log(Level.INFO, fieldDescriptors.toString());
        } catch (IllegalAccessException e) {
            logger.severe(e.getMessage());
        }
    }
}
