package ru.nlmk.study.jse24.var1;

public enum CollectType {
    FLAG_ONLY,
    LAST_VALUE_ONLY,
    FLAG_AND_LAST_VALUE
}
