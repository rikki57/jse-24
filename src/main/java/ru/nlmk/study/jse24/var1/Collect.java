package ru.nlmk.study.jse24.var1;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Collect {
    CollectType value();
}
